# AWS

## About `NotTriggerScaleUp` & `FailedScheduling` 

The problem consists of several sub-problems. It might require more updates later again.

The `sdl-dev` cluster was first configured that the cluster nodes including solr nodes run on the `us-east-1a` zones. The purpose was to make the `sdl-dev` cluster use other zones of the `us-east-1` region. The first problem was written on the Auto Scaling Groups (ASG) page as below:

```
Launching a new EC2 instance. Status Reason: We currently do not have sufficient im4gn.8xlarge capacity in the Availability Zone you requested (us-east-1a). Our system will be working on provisioning additional capacity. You can currently get im4gn.8xlarge capacity by not specifying an Availability Zone in your request or choosing us-east-1b, us-east-1c. Launching EC2 instance failed.
```

To fix the error, we went for `im4gn.xlarge` since It's just to get solr to run and create data and analyze the failover concept. However, we still have to adjust the resources after this change. 

Before adjusting the resources of the solr cloud spec and JVM arguments, I received the message below:

```
$ kubectl describe pod test-solrcloud-0
...
Events:
...
0/16 nodes are available: 1 node(s) had untolerated taint {cluster/workload: ingress}, 13 node(s) didn't match Pod's node affinity/selector, 16 Insufficient cpu, 16 Insufficient memory, 3 node(s) had untolerated taint {cluster/workload: cluster-infra}, 3 node(s) had untolerated taint {cluster/workload: zookeeper}, 3 node(s) had untolerated taint {node-role.kubernetes.io/control-plane: }. preemption: 0/16 nodes are available: 13 Preemption is not helpful for scheduling, 3 No preemption victims found for incoming pod.
...
```

I changed the resouce limits and JVM parameter:

```
    podOptions:
      resources:
        limits:
          cpu: 3
          memory: 10Gi
    ...
    ...
    solrJavaMem: "-Xms8g -Xmx8g"
    ...
    ...
```

Next to this, the problem followed another one as @sdlarsen pointed out that the local storage on those instance types is not compatible as the current local storage setup is only looking for raid devices.   

```
  Warning  FailedScheduling   48m                  default-scheduler   running PreBind plugin "VolumeBinding": binding volumes: timed out waiting for the condition
```

Simply changing the storageClassName to "standard-storage" (i.e. EBS (networked) storage) resolved the volume problem. 

```
...
      pvcTemplate:
        spec:
          storageClassName: "standard-storage"
          accessModes:
          - "ReadWriteOnce"
          resources:
            requests:
              storage: "20Gi"
...
```

Note: Storage settings didn't reflect after pushing the changes, so I turned off and on the solr cluster to see the new persistent state.

The last issue we have is that I request 6 solrs nodes, but the solr ASG is already fixed on three nodes and autoscaling is not enabled as decribed below: 

```
solr = {
      autoscale             = false,
      instance_role         = "solr",
      instance_type         = "im4gn.xlarge",
      max_instances         = 10,
      min_instances         = 3,
      node_labels           = "cluster/workload=solr",
      node_taints           = "cluster/workload=solr:NoSchedule",
      spot_price            = ""
      container_device_size = 0,
    }
```

```
$ kubectl get pod -ltechnology=solr-cloud
NAME               READY   STATUS    RESTARTS   AGE
test-solrcloud-0   1/1     Running   0          24m
test-solrcloud-1   1/1     Running   0          24m
test-solrcloud-2   1/1     Running   0          3m23s
test-solrcloud-3   0/1     Pending   0          3m23s
test-solrcloud-4   0/1     Pending   0          3m23s
test-solrcloud-5   0/1     Pending   0          3m23s
```

Thanks to @sdlarsen, by setting the `min_instances` to 6, we are able to see that the solr nodes are spread equally among three AZ's. 

![](images/autoscale-azs.png) 

```
solr = {
      autoscale             = false,
      instance_role         = "solr",
      instance_type         = "im4gn.xlarge",
      max_instances         = 10,
      min_instances         = 6,
      node_labels           = "cluster/workload=solr",
      node_taints           = "cluster/workload=solr:NoSchedule",
      spot_price            = ""
      container_device_size = 0,
    }
```


### Resources

1. https://www.datadoghq.com/blog/debug-kubernetes-pending-pods/
2. https://kubernetes.io/docs/setup/best-practices/multiple-zones/
3. https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#pod-topology-spread-constraints
4. https://kubernetes.io/docs/concepts/storage/storage-classes/
