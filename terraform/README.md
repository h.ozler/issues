# Terraform

The terraform-related problems I have had.

## The provider does not have a package available for your current platform, darwin_arm64

While terraform was downloading and generating files into the `/k8s-infra/terraform/environments/sdl-dev/terraform` directory, the following two problems arose:

```
╷
│ Error: Incompatible provider version
│
│ Provider registry.terraform.io/hashicorp/template v2.2.0 does not have a package available for your current platform, darwin_arm64.
│
│ Provider releases are separate from Terraform CLI releases, so not all providers are available for all platforms. Other versions of this provider may have different platforms
│ supported.
╵

╷
│ Error: Incompatible provider version
│
│ Provider registry.terraform.io/terraform-providers/ignition v1.2.1 does not have a package available for your current platform, darwin_arm64.
│
│ Provider releases are separate from Terraform CLI releases, so not all providers are available for all platforms. Other versions of this provider may have different platforms
│ supported.
╵
```

The above errors show that the template and ignition providers were deprecated and archived before the existence of M1 (darwin_arm64) releases. This affects the Mac M2 chip as well. The following steps fix the template problem :

```
$ brew install kreuzwerker/taps/m1-terraform-provider-helper
$ m1-terraform-provider-helper activate
$ m1-terraform-provider-helper install hashicorp/template -v v2.2.0
$ m1-terraform-provider-helper deactivate
```

The command `m1-terraform-provider-helper` helps us to install terraform providers for the Mac M1 chip into the `${HOME}/.terraform.d/plugins_backup/registry.terraform.io/hashicorp/template/` path. In our case, the provider 2.2.0 will be in the `...template/2.2.0/darwin_arm64` folder.

For the later problem, I simply downloaded the related [darwin_arm64](https://releases.hashicorp.com/terraform-provider-ignition/1.2.1/) file and save it to `${HOME}/.terraform.d/plugins_backup/registry.terraform.io/terraform-providers/ignition/1.2.1/darwin_arm64`.

Once all the providers are ready, we're able to run the command below to generate the cluster plan:

```
$ pwd 
k8s-infra/terraform/environments/sdl-dev/
$ cd terraform
$ rm -rf .terraform .terraform.lock.hcl
$ make plan-cluster
...
...
Terraform has been successfully initialized!
...
...
```

### References

1. https://discuss.hashicorp.com/t/template-v2-2-0-does-not-have-a-package-available-mac-m1/35099
2. https://github.com/kreuzwerker/m1-terraform-provider-helper
3. https://releases.hashicorp.com/terraform-provider-ignition/1.2.1/
